// Теоретичне питання
// - Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// + Створити просту HTML-сторінку з кнопкою "Знайти по IP".
// + Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// + Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// - під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const button = document.body.querySelector(".btn");
const urlIpUser = "https://api.ipify.org/?format=json";
const urlInfoUser = "http://ip-api.com/json/";

async function sendRequest(url, method = "GET", config) {
    const response = await fetch(url, {method, ...config});
    return response.json();
}

async function userInfo(ip) {
    try {
        const response = await sendRequest(`${urlInfoUser}${ip}?fields=status,message,continent,country,regionName,city,district,query`);
        console.log(response);
        return response;
    } catch (error) {
        console.error("Помилка запиту: ", error);
    }
}

async function renderInfoUser(ip) {
    try {
        // const userInfoData = await userInfo(ip); 
        const {
            continent="не має даних", 
            country="не має даних", 
            regionName="не має даних", 
            city="не має даних", 
            region="не має даних"
        } = await userInfo(ip); // переробила одразу з деконструкцією і значеннями по замовчуванню

        const container = document.createElement("div");
        container.classList.add("container");

        container.insertAdjacentHTML("beforeend", `
            <p>Континент: ${continent}</p>
            <p>Країна: ${country}</p>
            <p>Регіон: ${regionName}</p>
            <p>Місто: ${city}</p>
            <p>Район: ${region}</p>
        `);

        document.body.append(container);
    } catch (error) {
        console.error("Помилка відображення користувача", error);
    }
    
}

button.addEventListener("click", async () => {
    try {
        const {ip} = await sendRequest(urlIpUser); //одразу зробили деконструкцію і витягнули ip з об'єкта, що прийшов з сервера
        console.log(ip);
        renderInfoUser(ip);
    } catch (error) {
        console.error('Помилка запиту:', error);
    }
})







// варіант запиту з .then.catch
// button.addEventListener("click", () => {
//     sendRequest(urlIpUser)
//         .then(data => {
//             console.log(data);
//         })
//         .catch(error => {
//             console.error('Помилка запиту:', error);
//         });
// });

